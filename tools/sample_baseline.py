import codecs
import sys
import numpy
from scipy import stats
import random
import argparse

### read in baseline output
### compute Tagging accuracy; sample that many from gold; check if significantly better, test 1000 times
tokens=[]
gold=[]
pred=[]
correct=0
total=0
countInst=0

parser = argparse.ArgumentParser(description="Compute tagging accuracy for CoNLL file")
parser.add_argument('infile', help="input file in CoNLL format")
parser.add_argument('--ta', help="tagging accuracy")

args = parser.parse_args()
if not args.infile:
    print("specify a file")
    exit()


for line in codecs.open(args.infile):
    if line.strip():
        word,g,p = line.strip().split("\t")
        tokens.append(word)
        gold.append(g)
        pred.append(p)
    else:
        countInst+=1
        #insert sentence boundary markers
        gold.append("<S>")
        pred.append("<S>")
        tokens.append("<S>")


def accuracy(gold,pred):
    ta_instances=[]
    assert(len(gold)==len(pred))
    correct=0.0
    total=0.0
    inst_correct=0.0
    inst_total=0.0
    for g,p in zip(gold,pred):
        if g=="<S>":
            inst_ta=inst_correct/float(inst_total)
            ta_instances.append(inst_ta)
            inst_correct=0.0 #reset
            inst_total=0.0
   
            continue
        if g==p:
            correct+=1
            inst_correct+=1
        total+=1
        inst_total+=1

    accuracy=correct/float(total)
    print >>sys.stderr, "correct,total",correct, total
    return accuracy,ta_instances

baseline_ta,baseline_ta_inst = accuracy(gold,pred)
print >>sys.stderr, "baseline_ta, countInst", baseline_ta,countInst,len(baseline_ta_inst)
#print stats.ttest_rel()


possible_tags=["NOUN","VERB","ADJ","ADP","ADV","CONJ","DET","NUM","PRON","PRT","X","."]
k=10
gold=numpy.array(gold)
pred=numpy.array(pred)

if args.ta:
    baseline_ta=float(args.ta)
    print>>sys.stderr, "Using TA:", baseline_ta

#sum(np.random.binomial(1,0.93,20000)==1)/float(20000)
for i in range(0,1):
    #sample the amount of TA from gold
    print >>sys.stderr, "sampling size:", len(gold)-countInst
    indices=numpy.random.binomial(1,baseline_ta,len(gold)-countInst)
    print >>sys.stderr, "number 1's gold: {} {}".format(sum(indices==1)/float(len(indices)),sum(indices==1))
    print >>sys.stderr, "number 0's: {} {}".format(sum(indices==0)/float(len(indices)),sum(indices==0))
    current=[]
    idx=0
    print >>sys.stderr, "len instances",len(indices)
    cg=0
    cp=0
    for i in range(0,len(gold)):
        if gold[i] !="<S>":
            if indices[idx] ==1:
                current.append(gold[i])
                cg+=1
            else:
                # sample anything with is not gold
                possible = [t for t in possible_tags if t !=  gold[i]]
                #print len(possible) #must be 11
                selected = random.choice(possible)
                current.append(selected)
                #current.append(pred[i])
                cp+=1
            idx+=1
        else:
            current.append("<S>")
    print >>sys.stderr,  len(tokens), len(gold), len(current)
    print >>sys.stderr, "countgold/countpred",cg, cp
    assert(len(tokens)==len(gold))
    assert(len(gold)==len(current))
    for t,g,c in zip(tokens,gold,current):
        if c == "<S>":
            print ""
        else:
            print "{}\t{}\t{}".format(t,g,c)
    #print len(gold), len(current)
    ta,ta_per_inst= accuracy(gold,current)
    print >>sys.stderr, "current ta", ta
    #print "current ta", ta
    #s= stats.ttest_rel(baseline_ta_inst, ta_per_inst)
    #print "p-val", s[1]
    #print ""

    
