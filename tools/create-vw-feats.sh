#!/bin/bash
# @author: bplank
# create vw feature files from conll input data

usage()
{
cat <<EOF

usage: $0 options

Create rungsted (VW) feature files from conll input data

OPTIONS:
   -h      Show this message
   -d      data name [cpos|ptb]; default: cpos (universal pos)
   -c      cluster file
   -w      path to Wiktionary (optional)
   -o      output name suffix

EOF
}

DATA=cpos
#DATA=ptb
CLUSTER=
#LABELMAP=NO_LONGER_NEEDED
OUTSUFFIX=
while getopts "hc:w:d:o:" OPTION
do
    case "$OPTION" in
        h)
            usage
            exit 1
            ;;
        w)
            WIKTIONARY=$OPTARG
            ;;
	d)  
	    DATA=$OPTARG
	    ;;
	c)
	    CLUSTER=$OPTARG
	    ;;
	o)
	    OUTSUFFIX=$OPTARG
	    ;;
    esac
done

if [ -z "$IW_HOME" ]
  then
    echo "Variable IW_HOME is not defined"
    exit
fi

if [ -z "$DATA" ] 
then
    echo "PARAMETER MISSING!"
    usage
    exit 1
fi

DIR=$IW_HOME/data/
TDIR=$IW_HOME/tools
#cpos or ptb
#DATA=$1


# create training data
if [ -z $CLUSTER ] 
then 
### without cluster
    if [ -z "$WIKTIONARY" ]
    then
	python $TDIR/createfeats.py $DIR/$DATA/train/ontonotes-wsj-train.conll  > $DIR/$DATA/train/ontonotes-wsj-train.conll.vw.$OUTSUFFIX
    else
	python $TDIR/createfeats.py --training --wiktionary $WIKTIONARY $DIR/$DATA/train/ontonotes-wsj-train.conll  > $DIR/$DATA/train/ontonotes-wsj-train.conll.vw.$OUTSUFFIX
    fi



    FILES="eval/ontonotes-wsj-dev.conll eval/ontonotes-wsj-test.conll eval/gweb-emails-dev.conll eval/gweb-emails-test.conll eval/gweb-answers-test.conll eval/gweb-answers-dev.conll eval/gweb-newsgroups-test.conll eval/gweb-reviews-test.conll eval/gweb-newsgroups-dev.conll eval/gweb-reviews-dev.conll eval/gweb-weblogs-dev.conll eval/gweb-weblogs-test.conll"
    if [ -z "$WIKTIONARY" ]
    then
    # create dev/eval data
	for file in $FILES
	do
	    echo $file "no wiktionary"
	    python $TDIR/createfeats.py $DIR/$DATA/$file > $DIR/$DATA/$file.vw.$OUTSUFFIX
	done
    else
	for file in $FILES
	do
	    echo $file
	    echo "Wiktionary: " $WIKTIONARY
	    python $TDIR/createfeats.py --lower --wiktionary $WIKTIONARY  $DIR/$DATA/$file > $DIR/$DATA/$file.vw.$OUTSUFFIX
	done
	
    fi


else
### with cluster
    if [ -z "$WIKTIONARY" ]
    then
	python $TDIR/createfeats.py  --cluster $CLUSTER $DIR/$DATA/train/ontonotes-wsj-train.conll  > $DIR/$DATA/train/ontonotes-wsj-train.conll.vw.$OUTSUFFIX
    else
	# using constraints also at training and --training adds gold label to constraints
	#python $TDIR/createfeats.py --training --wiktionary $WIKTIONARY --cluster $CLUSTER $DIR/$DATA/train/ontonotes-wsj-train.conll  > $DIR/$DATA/train/ontonotes-wsj-train.conll.vw.$OUTSUFFIX
	python $TDIR/createfeats.py --cluster $CLUSTER $DIR/$DATA/train/ontonotes-wsj-train.conll  > $DIR/$DATA/train/ontonotes-wsj-train.conll.vw.$OUTSUFFIX
    fi
    


    FILES="eval/ontonotes-wsj-dev.conll eval/ontonotes-wsj-test.conll eval/gweb-emails-dev.conll eval/gweb-emails-test.conll eval/gweb-answers-test.conll eval/gweb-answers-dev.conll eval/gweb-newsgroups-test.conll eval/gweb-reviews-test.conll eval/gweb-newsgroups-dev.conll eval/gweb-reviews-dev.conll eval/gweb-weblogs-dev.conll eval/gweb-weblogs-test.conll"
    if [ -z "$WIKTIONARY" ]
    then
    # create dev/eval data
	for file in $FILES
	do
	    echo $file "no wiktionary"
	    python $TDIR/createfeats.py  --cluster $CLUSTER $DIR/$DATA/$file > $DIR/$DATA/$file.vw.$OUTSUFFIX
	done
    else
	for file in $FILES
	do
	    echo $file
	    echo "Wiktionary: " $WIKTIONARY
	    python $TDIR/createfeats.py --lower --wiktionary $WIKTIONARY --cluster $CLUSTER $DIR/$DATA/$file > $DIR/$DATA/$file.vw.$OUTSUFFIX
	done
	
    fi

fi