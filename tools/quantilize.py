import argparse
import pandas as pd
import sys

parser = argparse.ArgumentParser(description="Quantilize, threshold and scale importance weights from text classifier")
parser.add_argument('domain_prob_file', help="File with source and target probabilities")
parser.add_argument('vw_file', help="Data file in VW format", type=argparse.FileType('r'))
parser.add_argument('--quantiles', help="Number of quantiles for binning weights", default=5, type=int)
parser.add_argument('--lower', help="Lower threshold. Weights under this value are assigned an importance of 0 (this is no longer used)",
                    default=0, type=float)
parser.add_argument('--scale', help="Scale weights by this factor. Applied after thresholding. (also no longer used)", default=1, type=float)
args = parser.parse_args()

# Read weights
domain_probs = pd.read_csv(args.domain_prob_file, sep=" ", index_col=0, names=['target', 'source', 'ratio'])

# Defaults for weights below lower threshold
domain_probs['bin'] = -1
#domain_probs['importance'] = 1
domain_probs['importance'] = 0

# Quantilize and scale weights above threshold
above_threshold = domain_probs.target >= args.lower
assert(len(above_threshold)==len(domain_probs)) #now use all!
if args.quantiles > 0:
    domain_probs.bin[above_threshold] = pd.qcut(domain_probs[above_threshold].target, args.quantiles, labels=False)
    group_means = domain_probs[above_threshold].groupby('bin').target.mean().to_dict()
    domain_probs.importance[above_threshold] = [group_means[bin_id] for bin_id in domain_probs.bin[above_threshold]]
    domain_probs.importance[above_threshold] *= args.scale
    #domain_probs.importance[above_threshold] += 1.0
    print >>sys.stderr, group_means
else:
    #do not use quantiles
    domain_probs.importance = domain_probs.target
    domain_probs.importance *= args.scale

importance = domain_probs.importance.values

print >>sys.stderr, len(importance)
sentence_counter = 0
assert(len(importance) == len(domain_probs))
for line in args.vw_file:
    f = line.strip().split()
    if f:
        print "{} {} {}".format(f[0], importance[sentence_counter], " ".join(f[1:]))
    else:
        print ""
        sentence_counter += 1
