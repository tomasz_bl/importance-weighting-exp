DOMAIN=weblogs
ITERS=50
BASELINEPREDICTION=$IW_HOME/output/cpos/baseline/predictions/$DOMAIN.predicted
for ta in 0.95 0.951 0.952 0.953 0.954 0.955 0.956 0.957 0.958 0.959 0.960 0.961 0.962 0.963 0.964 0.965 0.966 0.967 0.968 0.969 0.970 
do
    echo "-----SAMPLE " $ta
    for i in `seq 1 $ITERS`
    do
	echo "SAMPLE-ITER" $ta $i
	python $IW_HOME/tools/sample_baseline.py --ta $ta $BASELINEPREDICTION > sample.$DOMAIN
	$LOWLANDS_HOME/tools/signf_pos/run-signf.sh $IW_HOME/data/cpos/eval/gweb-$DOMAIN-test.conll $BASELINEPREDICTION sample.$DOMAIN
    done
done