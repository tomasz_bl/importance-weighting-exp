DOMAIN=answers
ITERS=50
BASELINEPREDICTION=$IW_HOME/output/cpos/baseline/predictions/answers.predicted

for ta in 0.9345 0.935 0.936 0.937 0.938 0.939 0.940 0.941 0.942 0.943 0.944 0.945 0.946 0.947 0.948 
do
    echo "-----SAMPLE " $ta
    for i in `seq 1 $ITERS`
    do
	echo "SAMPLE-ITER" $ta $i
	python $IW_HOME/tools/sample_baseline.py --ta $ta $BASELINEPREDICTION > sample.$DOMAIN
	$LOWLANDS_HOME/tools/signf_pos/run-signf.sh $IW_HOME/data/cpos/eval/gweb-$DOMAIN-test.conll $BASELINEPREDICTION sample.$DOMAIN
    done
done