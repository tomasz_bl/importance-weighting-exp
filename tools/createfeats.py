# coding: utf-8
#!/usr/bin/env python
# @author: Barbara Plank
#
# usage: ./{script} FILE [CLUSTERFILE]
#
import codecs
import argparse
import logging
import sys
import re
from collections import defaultdict

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

parser = argparse.ArgumentParser(description="""Create POS features (vw format) from CoNLL tabular file""")
parser.add_argument('infile', help="input file in CoNLL format")
parser.add_argument('--cluster', help="Brown word cluster file")
parser.add_argument('--wiktionary', help="wiktionary constraints")
parser.add_argument('--lower', help="use lowercase lookup for wiktionary", action="store_true", default=False)
parser.add_argument('--training', help="use gold label as constraint only during training", action="store_true", default=False)



def main():    
    args = parser.parse_args()
    if not args.infile:
        print("specify a file")
        exit()

    if args.cluster:
        clusterfile= args.cluster
        #read clusters
        words2ids = {}
        CLUSTER=open(clusterfile)
        for l in CLUSTER:
            l=l.strip()
            fields = l.split("\t")
            clusterId = fields[0]
            word = fields[1]
            words2ids[word] = clusterId
        CLUSTER.close()

    dictionary = defaultdict(set)
    if args.wiktionary:
        for line in codecs.open(args.wiktionary, encoding='utf-8'):
            en, word, tag, _ = line.strip().split("\t")
            if args.lower:
                word = word.lower()
                dictionary[word].add(tag)
            else:
                dictionary[word].add(tag)
    

    FILE = codecs.open(args.infile,encoding="utf-8")
    sentid=0
    tokenid=0
    
    instances=[]
    instance=[] 
    # read in file
    for l in FILE:
        line = l.strip()
        fields=l.split()        
        assert(len(fields)!=1)

        if fields:
            #tuple: sentid, tokenid, inputline
            instance.append((sentid,tokenid,line))
            tokenid+=1
        else:
            instances.append(instance)
            instance=[]
            tokenid=0
            sentid+=1

    #add last one
    if instance:
        instances.append(instance)
    
    FILE.close()

    for instance in instances:
        words=[]
        tags=[]
        tokenids=[]
        for sentid,tokenid,line in instance:
            fields=line.split()
            word = fields[0]
            #rungsted requires that tokens don't contain :
            if ":" in word:
                word =word.replace(":","COLON")
            label = fields[-1]
            if ":" in label:
                label=label.replace(":","COLON")
            if label == "''":
                label=label.replace("''","STARTQ")
                
            words.append(word)
            tags.append(label)
            tokenids.append(tokenid)
        assert(len(words)==len(tags))
        
        
        for tokenid,word,label in zip(tokenids,words,tags): 
            #replace any number with 0
            orgword = word
            word = re.sub(r'\d',"0",word)
            word = word.lower() #user lowercase

            feats=[]    

            # token
            feats.append(u"w={}".format(word))

            # 1-4 char suffix & prefix
            for i in xrange(1,4+1):
                if len(word)>i:
                    feats.append(u"pre{}={}".format(i,word[0:i]))
                    feats.append(u"suf{}={}".format(i,word[-i:]))

            # check capitalization
            if orgword[0].isupper():
                feats.append("U+") #first char is uppercase

            # check if contains digits
            if "0" in word:
                feats.append("D+")

            # check if contains hyphen
            if "-" in word:
                feats.append("H+")

                
            # # if entire word is uppercase
            if orgword.isupper():
                feats.append("UA+") 


            #prev token
            if tokenid>0:
                prev=words[tokenid-1]
                feats.append(u"prev={}".format(prev))
            #w-2 token
            if tokenid>1:
                prev=words[tokenid-2]
                feats.append(u"pprev={}".format(prev))

            #next token
            if tokenid<len(words)-1:
                nex=words[tokenid+1]
                feats.append(u"next={}".format(nex))
            #w+2 token
            if tokenid<len(words)-2:
                nnex=words[tokenid+2]
                feats.append(u"nnext={}".format(nnex))

            # # brown cluster
            if args.cluster:
                clusterid = words2ids.get(word)
                if clusterid:
                    feats.append("|c") #cluster namespace
                    feats.append("c2="+clusterid[0:2]) #prefix 2
                    feats.append("c4="+clusterid[0:4]) #prefix 4
                    feats.append("c6="+clusterid[0:6]) #prefix 6
                    feats.append("c8="+clusterid[0:8]) #prefix 8
                    feats.append("c10="+clusterid[0:10]) #prefix 10
                    feats.append("c12="+clusterid[0:12]) #prefix 12
                    feats.append("c14="+clusterid[0:14]) #prefix 14
                    feats.append("c16="+clusterid[0:16]) #prefix 16

            #print "%s %s %s" % (word," ".join(feats),label) 
            if not args.wiktionary:
                print u"{0} '{1}-{2}|b {3}".format(label,sentid,tokenid," ".join(feats))
            else:
                word = words[tokenid]
                #print word, dictionary[word]
                #list_tags=["?{}".format(label2id[tag]) for tag in dictionary[word] if tag in label2id]
                list_tags=["?{}".format(tag) for tag in dictionary[word]]
                if args.training:
                    if label not in list_tags:
                        list_tags.append("?{}".format(label))
                possible_tags=" ".join(set(list_tags))

                if possible_tags.strip():
                    print u"{0} {4} '{1}-{2}|b {3}".format(label,sentid,tokenid," ".join(feats),possible_tags)
                else:
                    print u"{0} '{1}-{2}|b {3}".format(label,sentid,tokenid," ".join(feats))

        print


if __name__=="__main__":
    main()
