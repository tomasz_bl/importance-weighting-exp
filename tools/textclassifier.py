#!/usr/bin/python
#@author: bplank
#LogReg model to get weights for SRC training data
import codecs
import sys
import re
import numpy as np
from sklearn.feature_extraction.dict_vectorizer import DictVectorizer
from sklearn.preprocessing import LabelEncoder
from sklearn.linear_model import LogisticRegression
from sklearn.cross_validation import KFold
from sklearn.metrics import classification_report, confusion_matrix
from collections import Counter
import argparse

def ngrams(tokens, MIN_N, MAX_N):
    n_tokens = len(tokens)
    for i in xrange(n_tokens):
        for j in xrange(i+MIN_N, min(n_tokens, i+MAX_N)+1):
            yield tokens[i:j]

def read_data(filename,label,features,lower=False,input=None,labels=None,topk=-1):
    if input is None:
        input=[]
    if labels is None:
        labels=[]

    tmpinput=[]
    tmplabels=[]
    for line in codecs.open(filename,encoding="utf-8"):
        if not line.strip():
            continue
        
        words=line.strip().split()
        if lower:
            words = map(unicode.lower,words)
        if "nonalpha" in features:
            words_only_nonalpha=[]
            for w in words:
                if not re.match('^[a-zA-Z0-9_]+$',w): 
                    words_only_nonalpha.append(w)
            words = words_only_nonalpha

        feats=[]
        if "unigram" in features:
            for gram in ngrams(words,1,1):
                feats.append(u"G {}".format(" ".join(gram)))
        if "bigram" in features:
            for gram in ngrams(words,2,2):
                feats.append(u"G {}".format(" ".join(gram)))
        if "trigram" in features:
            for gram in ngrams(words,3,3):
                feats.append(u"G {}".format(" ".join(gram)))
        if "fourgram" in features:
            for gram in ngrams(words,4,4):
                feats.append(u"G {}".format(" ".join(gram)))
        if not ("unigram" or "bigram" or "trigram" or "fourgram" in features):
            raise Exception("unknown feature!")
        if topk==-1:
            labels.append(label)
            input.append(Counter(feats))
        else:
            tmplabels.append(label)
            tmpinput.append(Counter(feats))
        assert(len(input)==len(labels))

    if topk!=-1:
        #select only topk target instances
        if topk<len(tmplabels):
            all_idx = range(0,topk)
        else:
            all_idx = range(0,len(tmplabels))
            print >>sys.stderr, "taking all since trg is smaller than src"
        selected_idx = np.random.permutation(all_idx)
        print >>sys.stderr, len(selected_idx)
        sel_input= list(np.array(tmpinput)[selected_idx])
        sel_labels = list(np.array(tmplabels)[selected_idx])
        input.extend(sel_input)
        labels.extend(sel_labels)
    return input,labels


def main():
    parser = argparse.ArgumentParser(description="LogReg model to discriminate between two domains")
    parser.add_argument('datasrc', help="src input txt file")
    parser.add_argument('datatrg', help="trg input txt file")
    parser.add_argument('--features', help="[unigram|bigram|trigram|fourgram]",default="unigram")
    parser.add_argument('--lower', help="lowercase tokens", action="store_true", default=False)
    parser.add_argument('--sample', help="if trg is much larger, sample same amount of instances from it as src", action="store_true", default=False)
    args = parser.parse_args()
    #try:
        
    #except:
    #    print "Specify a src and trg file!"
    #    exit()
    
    print >>sys.stderr, "Features: {}".format(args.features)
        
    #read src
    input, labels = read_data(args.datasrc,"src", args.features,lower=args.lower)
    print >>sys.stderr, len(input)

    topk=-1 #use all trg
    if args.sample:
        print >> sys.stderr, "Sampling from target to match src # of instances"
        topk=len(input)
    #read trg
    input, labels = read_data(args.datatrg,"trg",args.features,lower=args.lower, input=input,labels=labels,topk=topk)
    print >>sys.stderr, len(input)

    # create data object for sklearn
    # labels must be numeric, use the LabelEncoder to get mapping
    data ={}

    #map label to ids
    labEnc = LabelEncoder()
    labEnc.fit(labels)
    print >> sys.stderr, "possible labels: {}".format(labEnc.classes_)
    print >> sys.stderr, "number of instances: {}".format(len(labels))

    # target labels
    data['target'] = labEnc.transform(labels)
    data['DESC'] = "binary text classification"
    data['target_names'] = labEnc.classes_

    # now vectorize data and save as 'data'


    #vectorizer = CountVectorizer()
    # from text to features... (by default word n-grams) stored in sparse format
    #data['data'] = vectorizer.fit_transform(input)


    vectorizer = DictVectorizer()
    data['data'] = vectorizer.fit_transform(input)
    #print >>sys.stderr,vectorizer.vocabulary_
    print >>sys.stderr, "size of vocab: {}".format(len(vectorizer.vocabulary_))

    weights=[]
    indices=[]

    # get weights for source by doing 5fold CV
    crossvalid = KFold(len(data["target"]), n_folds=5, indices=True,shuffle=True,random_state=12345)
    fold=0
    for train_indices, test_indices in crossvalid:
        print >>sys.stderr,"fold: {}".format(fold)
        fold+=1
        #print >>sys.stderr, "test_indices:", test_indices
        indices.append(test_indices)
        # train part (all except test part)
        X_train = data['data'][train_indices]
        y_train = data['target'][train_indices]

        # test part
        X_test = data['data'][test_indices]
        y_test = data['target'][test_indices]

        # output statistics
        print >>sys.stderr, "#inst train: %s" % (len(y_train))
        print >>sys.stderr, "#inst test: %s" % (len(y_test))

        logistic = LogisticRegression(class_weight='auto')
        logistic.fit(X_train,y_train)

        ## top-k features
        k=20
        if hasattr(logistic, 'coef_'):
            feature_names = np.asarray(vectorizer.get_feature_names())
            print >>sys.stderr, ">>SRC:"
            top_src = np.argsort(logistic.coef_[0])[0:k]
            for f in feature_names[top_src]:
                print >> sys.stderr, f
            print >>sys.stderr, ">>TRG:"
            top_trg = np.argsort(logistic.coef_[0])[-k:]
            for f in feature_names[top_trg]:
                print >> sys.stderr, f
            #top10_trg = np.argsort(logistic.coef_[1])[-10:]

        #print logistic
        y_pred_prob= logistic.predict_proba(X_test)
        y_pred= logistic.predict(X_test)

        #print >>sys.stderr,"Pred:", y_pred
        #print >>sys.stderr,"prob:", y_pred_prob
        #pl.scatter()
        #for y_num in y_pred:
        #    print >>sys.stderr,y_num, labEnc.inverse_transform(y_num)
        for prob,pred,gold,idx in zip(y_pred_prob,y_pred,y_test,test_indices):
            p_src=prob[0]
            p_trg=prob[1]
            p_t_s=p_trg/float(p_src)

            #print >>sys.stderr,prob[0],prob[1],prob[0]/prob[1], pred, gold
            # keep only weight for source instances of this fold
            if gold == labEnc.transform(["src"])[0]:
                weights.append((idx,p_trg,p_src,p_t_s))

        #print "Gold:", y_test
        #print >>sys.stderr,0, labEnc.inverse_transform(0)
        #print >>sys.stderr,1, labEnc.inverse_transform(1)
        # get accuracies
        print >> sys.stderr, classification_report(y_test, y_pred, target_names=data['target_names'])

    #output weights: idx, prob_target, prob_source, ratio target/source
    for idx,p_trg,p_src,p_t_s in sorted(weights):
        print idx,p_trg,p_src,p_t_s

if __name__=="__main__":
    main()
