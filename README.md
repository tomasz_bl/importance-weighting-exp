Importance Weighting experiments
================================

This repository contains the tools and script for the experiments reported in the following paper:

````
@inproceedings{Plank:ea:2014:EMNLP,
	Author = {Barbara Plank, Anders Johannsen and Anders Søgaard},
	Booktitle = {EMNLP}
	Title = {Importance weighting and unsupervised domain adaptation of POS taggers: A negative result},
	Address = {Doha, Qatar},
	Year = {2014}}
````

## Setup:

For the experiments, set the IW_HOME variable, e.g.

```
export IW_HOME=/home/bplank/lowlands/importance-weighting-exp
```

Moreover, you will have to download ``rungsted``, the fast
perceptron-based tagger [1].  We here use a fork of the original
repository, and put it under the ``tools`` folder. To get the tagger,
perform the following steps. Note that the experiments were done with
the *pydecode* branch of this repository, thus make sure you use that.

```
mkdir tools
cd tools
git clone https://github.com/bplank/rungsted
cd rungsted 
git checkout pydecode
sh clean.sh
sh build.sh
git branch --list ## this should show you an * in front of pydecode (not master)

```
 
## Data:

You will need the SANCL training and evaluation data. To create the
data sets, see the `` data/cp-gwebdata.sh`` script and adjust the path
to the gweb folder (PATHGWEB). After running the script, you should
have two folders for fine-grained (ptb) and coarse-grained (cpos) POS
tags, with the following structure (the example is for cpos, for ptb
it is the same structure where cpos=ptb):

```
> ls cpos/eval/*conll
cpos/eval/gweb-answers-dev.conll      cpos/eval/gweb-reviews-dev.conll
cpos/eval/gweb-answers-test.conll     cpos/eval/gweb-reviews-test.conll
cpos/eval/gweb-emails-dev.conll       cpos/eval/gweb-weblogs-dev.conll
cpos/eval/gweb-emails-test.conll      cpos/eval/gweb-weblogs-test.conll
cpos/eval/gweb-newsgroups-dev.conll   cpos/eval/ontonotes-wsj-dev.conll
cpos/eval/gweb-newsgroups-test.conll  cpos/eval/ontonotes-wsj-test.conll
> ls cpos/train/*conll
cpos/train/ontonotes-wsj-train.conll
```

Moreover, the script will prepare the unlabeled data folder in
``data/unlabeled``.


## Test the tagger

To test whether the tagger works, try to launch ``sh
src/test-rungsted.sh``. This will train the tagger and evaluate it on
the answers test set. You should get an accuracy of 0.9341.

## Baselines

To get the baseline results for fine-grained and coarse-grained pos,
see the respective scripts in ``src/[ptb|cpos]/baseline.sh``. For
instance, by running the following command you should get back the
results in Table 1 that compare our system (using fine-grained POS)
with prior work on the SANCL test sets.

```
sh src/ptb/baseline.sh
```


## Experiments

For the experiments, see the ``run.sh`` script, which will exceute the
respective scripts in the ``src/`` folder.


### References:

[1] ``https://github.com/coastalcph/rungsted`` by Anders Johannsen