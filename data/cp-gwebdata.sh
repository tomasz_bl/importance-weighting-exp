## set dir to SANCL data
PATHGWEB=~/lowlands/data/corpora/gweb

mkdir -p cpos/train
mkdir -p cpos/eval
mkdir -p ptb/train
mkdir -p ptb/eval

## train file
less $PATHGWEB/sancl_data_train_dev/train/ontonotes-wsj-train.conll | awk '{if (NF>0) printf "%s\t%s\n", $2,$4; else print }' > cpos/train/ontonotes-wsj-train.conll
less $PATHGWEB/sancl_data_train_dev/train/ontonotes-wsj-train.conll | awk '{if (NF>0) printf "%s\t%s\n", $2,$5; else print }' > ptb/train/ontonotes-wsj-train.conll



## put dev and test files in eval folder
for file in gweb-emails-dev.conll gweb-weblogs-dev.conll ontonotes-wsj-dev.conll 
do
    less $PATHGWEB/sancl_data_train_dev/dev/$file | awk '{if (NF>0) printf "%s\t%s\n", $2,$4; else print }' > cpos/eval/$file
    less $PATHGWEB/sancl_data_train_dev/dev/$file | awk '{if (NF>0) printf "%s\t%s\n", $2,$5; else print }' > ptb/eval/$file
done
for file in $PATHGWEB/sancl_data_eval/eval/*conll
do
    less $PATHGWEB/sancl_data_eval/eval/`basename $file` | awk '{if (NF>0) printf "%s\t%s\n", $2,$4; else print }' > cpos/eval/`basename $file`
    less $PATHGWEB/sancl_data_eval/eval/`basename $file` | awk '{if (NF>0) printf "%s\t%s\n", $2,$5; else print }' > ptb/eval/`basename $file`
done

## unlabeled data
less $PATHGWEB/sancl_data_train_dev/train/ontonotes-wsj-train.conll | awk '{if (NF>0) printf "%s ", $2; else print ""}' > unlabeled/ontonotes.unlabeled.txt

cp $PATHGWEB/sancl_data_train_dev/train/*.unlabeled.txt unlabeled
cd unlabeled
sh process-unlab.sh
cd ..
