import sys
import codecs
from collections import defaultdict
wiktionary=defaultdict(list)
for line in codecs.open(sys.argv[1]):
    fields = line.strip().split("\t")
    word=fields[1]
    tag=fields[2]
    wiktionary[word].append(tag)
def isopenclass(tags,word):
    if "VERB" in tags:
        return True
    if "NOUN" in tags:
        return True
    if "ADJ" in tags:
        return True
    if "ADV" in tags and word.endswith("ly"):
        return True
    else:
        return False

onlyOpenclass=False
if len(sys.argv) == 4:
    onlyOpenclass=True
#print wiktionary
for line in codecs.open(sys.argv[2]):
    fields = line.strip().split()
    out=""
    for token in fields:
        if token in wiktionary:
            tags=wiktionary[token]
            orgtoken=token
                
            if len(tags)>1:
                token = "|".join(sorted(wiktionary[token]))
            else:
                token=tags[0]
            if onlyOpenclass and not isopenclass(tags,token):
                token=orgtoken
        else:
            token="NA" 
        out+=token+" "
    print out.strip()


