#!/bin/bash

#### PARAMETERS 
DATA=cpos
SUFFIX=base

WEIGHTS=$1
#answers
DOMAIN=$2

#####

CLUSTER=$IW_HOME/data/wordclusters/alldomains.3-100.mst.k5-c1000-p1.out-paths

DIR=$IW_HOME/data/$DATA
TRAININ=$DIR/train/ontonotes-wsj-train.conll.vw.$SUFFIX

PASSES=5
LOWER=0

OUTPUT=output/cpos/wran_`basename $WEIGHTS`
MODELDIR=$OUTPUT/models
mkdir -p $OUTPUT
mkdir -p $OUTPUT/train
TRAIN=$OUTPUT/train/ontonotes-wsj-train.conll.vw.weights
MODEL=$MODELDIR/`basename $TRAIN`.model.weighted
####

mkdir -p $MODELDIR

for QUANTILES in 0 5 10 20 50 100
do
    for SCALE in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 20 32 
    do 
	echo "SCALE-QUANTILES:" $SCALE $QUANTILES 1>&2
	python tools/quantilize.py --scale $SCALE --lower $LOWER --quantiles $QUANTILES $WEIGHTS $TRAININ > $TRAIN
	cat $TRAIN | cut -d' ' -f2 | sort |uniq -c 1>&2
	python tools/rungsted/rungsted/labeler.py --train $TRAIN --final-model $MODEL --passes $PASSES --no-ada-grad
	
	for f in test dev
	do
	    python tools/rungsted/rungsted/labeler.py --initial-model $MODEL --test $DIR/eval/gweb-$DOMAIN-$f.conll.vw.base  --predictions /tmp/$$.pred
	    python tools/accuracy_conll.py /tmp/$$.pred
	done
    done
done


