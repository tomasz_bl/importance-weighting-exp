#!/bin/bash

#### to get idea how sensitive it is to number of iterations = PASSES

#### PARAMETERS 
DATA=cpos
CLUSTER=$IW_HOME/data/wordclusters/alldomains.3-100.mst.k5-c1000-p1.out-paths
WIKTIONARY=$IW_HOME/data/extendedwiktionary/en.tags.li.onto

#SUFFIX=base+wik
SUFFIX=base
DIR=$IW_HOME/data/$DATA
TRAIN=$DIR/train/ontonotes-wsj-train.conll.vw.$SUFFIX
LEARNINGRATE=1.0

#PASSES=5

OUTPUT=output/cpos/baseline
MODELDIR=$OUTPUT/models
MODEL=$MODELDIR/`basename $TRAIN`.model

####

#create vw feature files 
./tools/create-vw-feats.sh -d $DATA -c $CLUSTER -o $SUFFIX 

mkdir -p $MODELDIR
mkdir -p $OUTPUT


for ITER in `seq 1 20`
do
    echo "ITER:", $ITER
    python tools/rungsted/rungsted/labeler.py --train $TRAIN --final-model $MODEL --passes $ITER --no-ada-grad

    for DOMAIN in answers reviews emails weblogs newsgroups
    do
    #./eval-test.sh $MODEL $DIR $LABELS $PASSES $SUFFIX $OUTPUT $TRAINUNLAB $DOMAIN
	python tools/rungsted/rungsted/labeler.py --initial-model $MODEL --test $DIR/eval/gweb-$DOMAIN-dev.conll.vw.base  --predictions /tmp/$$.pred
	python tools/accuracy_conll.py /tmp/$$.pred
	rm /tmp/$$.pred
    done

    echo "wsj" #ontonotes-wsj-test.conll
    python tools/rungsted/rungsted/labeler.py --initial-model $MODEL --test $DIR/eval/ontonotes-wsj-dev.conll.vw.base  --predictions /tmp/$$.pred
    python tools/accuracy_conll.py /tmp/$$.pred
    rm /tmp/$$.pred

    echo "=============="
done


