
mkdir -p data/weights

#for file in answers emails newsgroups reviews weblogs  
for file in weblogs
do
    echo $file

    #for ending in brown.v2 brown.v2.open wik.v2 wik.v2.open
    for ending in brown.v2 wik.v2
    do
     	nohup python tools/textclassifier.py --features unigram data/unlabeled/ontonotes.unlabeled.txt.utf8.$ending data/unlabeled/gweb-$file.unlabeled.txt.utf8.$ending 2> data/weights/log.$file.w.unigram.$ending > data/weights/w.$file.unigram.$ending &
     	nohup python tools/textclassifier.py --features bigram data/unlabeled/ontonotes.unlabeled.txt.utf8.$ending data/unlabeled/gweb-$file.unlabeled.txt.utf8.$ending 2> data/weights/log.$file.w.bigram.$ending > data/weights/w.$file.bigram.$ending &
     	nohup python tools/textclassifier.py --features trigram data/unlabeled/ontonotes.unlabeled.txt.utf8.$ending data/unlabeled/gweb-$file.unlabeled.txt.utf8.$ending 2> data/weights/log.$file.w.trigram.$ending > data/weights/w.$file.trigram.$ending &
     	nohup python tools/textclassifier.py --features fourgram data/unlabeled/ontonotes.unlabeled.txt.utf8.$ending data/unlabeled/gweb-$file.unlabeled.txt.utf8.$ending 2> data/weights/log.$file.w.fourgram.$ending > data/weights/w.$file.fourgram.$ending &
    done

    nohup python tools/textclassifier.py --features unigram data/unlabeled/ontonotes.unlabeled.txt.utf8 data/unlabeled/gweb-$file.unlabeled.txt.utf8 2> data/weights/log.$file.w.unigram.tok > data/weights/w.$file.unigram.tok &
    python tools/textclassifier.py --features bigram data/unlabeled/ontonotes.unlabeled.txt.utf8 data/unlabeled/gweb-$file.unlabeled.txt.utf8 2> data/weights/log.$file.w.bigram.tok > data/weights/w.$file.bigram.tok &
    nohup python tools/textclassifier.py --features trigram data/unlabeled/ontonotes.unlabeled.txt.utf8 data/unlabeled/gweb-$file.unlabeled.txt.utf8 2> data/weights/log.$file.w.trigram.tok > data/weights/w.$file.trigram.tok &
    nohup python tools/textclassifier.py --features fourgram data/unlabeled/ontonotes.unlabeled.txt.utf8 data/unlabeled/gweb-$file.unlabeled.txt.utf8 2> data/weights/log.$file.w.fourgram.tok > data/weights/w.$file.fourgram.tok 

done


