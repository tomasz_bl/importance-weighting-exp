#!/bin/bash
if [ -z "$IW_HOME" ]
  then
    echo "Variable IW_HOME is not defined"
    exit
fi

#### PARAMETERS 
DATA=cpos

CLUSTER=$IW_HOME/data/wordclusters/alldomains.3-100.mst.k5-c1000-p1.out-paths
SUFFIX=base
DIR=$IW_HOME/data/$DATA
TRAIN=$DIR/train/ontonotes-wsj-train.conll.vw.$SUFFIX
PASSES=5

####

#create vw feature files 
./tools/create-vw-feats.sh -d $DATA -c $CLUSTER -o $SUFFIX 

DOMAIN=answers
python tools/rungsted/rungsted/labeler.py --train $TRAIN --passes $PASSES --no-ada-grad --test $DIR/eval/gweb-$DOMAIN-test.conll.vw.$SUFFIX


