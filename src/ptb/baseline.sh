#!/bin/bash

#### PARAMETERS 
DATA=ptb
CLUSTER=$IW_HOME/data/wordclusters/alldomains.3-100.mst.k5-c1000-p1.out-paths
SUFFIX=base
DIR=$IW_HOME/data/$DATA
TRAIN=$DIR/train/ontonotes-wsj-train.conll.vw.$SUFFIX


PASSES=5

OUTPUT=output/$DATA/baseline
MODELDIR=$OUTPUT/models
MODEL=$MODELDIR/`basename $TRAIN`.model

####

#create vw feature files 
./tools/create-vw-feats.sh -d $DATA -c $CLUSTER -o $SUFFIX 

mkdir -p $MODELDIR
mkdir -p $OUTPUT

python tools/rungsted/rungsted/labeler.py --train $TRAIN --final-model $MODEL --passes $PASSES --no-ada-grad --labels data/ptb/ptb.labels

for DOMAIN in answers reviews emails weblogs newsgroups
do
    
    python tools/rungsted/rungsted/labeler.py --initial-model $MODEL --test $DIR/eval/gweb-$DOMAIN-test.conll.vw.base  --predictions /tmp/$$.pred
    python tools/accuracy_conll.py /tmp/$$.pred
    rm /tmp/$$.pred
done

echo "wsj" #ontonotes-wsj-test.conll
python tools/rungsted/rungsted/labeler.py --initial-model $MODEL --test $DIR/eval/ontonotes-wsj-test.conll.vw.base  --predictions /tmp/$$.pred
python tools/accuracy_conll.py /tmp/$$.pred
rm /tmp/$$.pred



