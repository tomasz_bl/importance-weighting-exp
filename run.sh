
### baseline: sensitivity to iterations on dev sets
mkdir -p results/cpos
mkdir -p results/cpos/tables
#nohup sh src/cpos/baseline-iters.sh > results/cpos/nohup-baseline-iters.out 2> results/cpos/nohup-baseline-iters.outerr &
#less results/cpos/nohup-baseline-iters.out | python src/grep-acc.py > results/cpos/tables/baseline-iters.dat
###

### baselines on test sets (coarse and fine)

#Table 1:
#sh src/ptb/baseline.sh
###

#Table 3:
#sh src/cpos/baseline.sh
#sh src/cpos/baseline+wiktionary-constr.sh
#sh $LOWLANDS_HOME/tools/signf_pos/run-signf.sh data/cpos/eval/gweb-reviews-test.conll output/cpos/baseline/predictions/reviews.predicted output/cpos/baseline+wik/predictions/reviews.predicted

######


# Analysis POS see:  https://bitbucket.org/bplank/myconllutils
#python bigramposdispersion.py -s ~/lowlands/importance-weighting-exp/data/cpos/train/ontonotes-wsj-train.conll -t ~/lowlands/importance-weighting-exp/data/cpos/eval/gweb-newsgroups-test.conll






