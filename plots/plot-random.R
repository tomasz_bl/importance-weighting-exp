pdf("rrandom.pdf",family="Palatino", pointsize=14,height=6,width=8)
par(oma=c(1,1.1,1,0.5))  # bottom, left, top, right
par(mar=c(2.1,1.1,1.1,1.1))
par(mfrow=c(3,5))
#par(font=2) #bold font
par(lwd=2) #bold lines




dataS=read.table("../results/cpos/random_sample.results",header=T)
dataE=read.table("../results/cpos/random_stdexp.results",header=T)
dataZ=read.table("../results/cpos/random_zipf3.results",header=T)

####
plot(dataS$answers,ylab="TA",main="answers",ylim=c(min(dataS$answers),94.0),col="gray")
abline(h=93.41)
abline(h=93.90,lty=2)

#legend("topright",c("wsj","answers","reviews","emails","weblogs","newsgroups"),pch=c(pchB,pchA,pchR,pchE,pchW,pchN),col=c(colB,colA,colR,colE,colW,colN),box.lwd = 0,ncol=2)
legend("topleft",c("random","exp","zipf"),col=c("gray","green","red"),pch=c(1,2,3))

plot(dataS$reviews,ylab="TA",main="reviews",ylim=c(min(dataS$reviews),94.9),col="gray")
abline(h=94.44)
abline(h=94.85,lty=2)


plot(dataS$emails,ylab="TA",main="emails",ylim=c(min(dataS$emails),94.15),col="gray")
abline(h=93.54)
abline(h=94.10,lty=2)


plot(dataS$weblogs,ylab="TA",main="weblogs",ylim=c(min(dataS$weblogs),95.4),col="gray")
abline(h=94.81)
abline(h=95.3,lty=2)


plot(dataS$newsgroups,ylab="TA",main="newsgroups",ylim=c(min(dataS$newsgroups),95.15),col="gray")
abline(h=94.55)
abline(h=95.1,lty=2)

#####


plot(dataE$answers,ylab="TA",main="answers",ylim=c(min(dataE$answers),94.0),col="green",pch=2)
abline(h=93.41)
abline(h=93.90,lty=2)

plot(dataE$reviews,ylab="TA",main="reviews",ylim=c(min(dataE$reviews),94.9),col="green",pch=2)
abline(h=94.44)
abline(h=94.85,lty=2)


plot(dataE$emails,ylab="TA",main="emails",ylim=c(min(dataE$emails),94.15),col="green",pch=2)
abline(h=93.54)
abline(h=94.10,lty=2)


plot(dataE$weblogs,ylab="TA",main="weblogs",ylim=c(min(dataE$weblogs),95.4),col="green",pch=2)
abline(h=94.81)
abline(h=95.3,lty=2)


plot(dataE$newsgroups,ylab="TA",main="newsgroups",ylim=c(min(dataE$newsgroups),95.15),col="green",pch=2)
abline(h=94.55)
abline(h=95.1,lty=2)

#####


####
plot(dataZ$answers,ylab="TA",main="answers",ylim=c(min(dataZ$answers),94.0),col="red",pch=3)
abline(h=93.41)
abline(h=93.90,lty=2)

plot(dataZ$reviews,ylab="TA",main="reviews",ylim=c(min(dataZ$reviews),94.9),col="red",pch=3)
abline(h=94.44)
abline(h=94.85,lty=2)


plot(dataZ$emails,ylab="TA",main="emails",ylim=c(min(dataZ$emails),94.15),col="red",pch=3)
abline(h=93.54)
abline(h=94.10,lty=2)


plot(dataZ$weblogs,ylab="TA",main="weblogs",ylim=c(min(dataZ$weblogs),95.4),col="red",pch=3)
abline(h=94.81)
abline(h=95.3,lty=2)


plot(dataZ$newsgroups,ylab="TA",main="newsgroups",ylim=c(min(dataZ$newsgroups),95.15),col="red",pch=3)
abline(h=94.55)
abline(h=95.1,lty=2)

#####



dev.off()