pdf("epochsdev.pdf",family="Palatino", pointsize=14)
par(oma=c(1,1.1,1,0.5))  # bottom, left, top, right
par(mar=c(2.1,1.1,1.1,1.1))
par(lwd=2)

data=read.table("../results/cpos/tables/baseline-iters.dat",header=T)

colB="black"
pchB=1

colA="red"
pchA=2
colR="green"
pchR=3
colE="blue"
pchE=4
colW="orange"
pchW=5
colN="gray"
pchN=6


plot(data$iter,data$wsj,pch=1,t="b",ylab="TA",xlab="epochs",ylim=c(92,99),xlim=c(0,20))
points(data$iter,data$answers,pch=pchA,t="b",ylab="TA",xlab="epochs",col=colA)
points(data$iter,data$reviews,pch=pchR,t="b",ylab="TA",xlab="epochs",col=colR)
points(data$iter,data$emails,pch=pchE,t="b",ylab="TA",xlab="epochs",col=colE)
points(data$iter,data$weblogs,pch=pchW,t="b",ylab="TA",xlab="epochs",col=colW)
points(data$iter,data$newsgroups,pch=pchN,t="b",ylab="TA",xlab="epochs",col=colN)
#abline(v=10,lty=2)
legend("topright",c("wsj","answers","reviews","emails","weblogs","newsgroups"),pch=c(pchB,pchA,pchR,pchE,pchW,pchN),col=c(colB,colA,colR,colE,colW,colN),box.lwd = 0,ncol=2)
dev.off()
